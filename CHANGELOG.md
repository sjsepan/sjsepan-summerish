# Summerish Color Theme - Change Log

## [0.2.2]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.2.1]

- update readme and screenshot

## [0.2.0]

- fix titlebar border, FG/BG in custom mode
- define window border in custom mode
- fix sidebar header separator FG
- generate new GUID

## [0.1.1]

- fix manifest and pub WF

## [0.1.0]

- buttons border
- fix manifest repo links
- consistent badges
- retain v0.0.3 for those that prefer earlier style

## [0.0.3]

- fix scrollbar, minimap slider transparencies:
light:
"minimapSlider.activeBackground": "#00000040",
"minimapSlider.background": "#00000020",
"minimapSlider.hoverBackground": "#00000060",
"scrollbarSlider.activeBackground": "#00000040",
"scrollbarSlider.background": "#00000020",
"scrollbarSlider.hoverBackground": "#00000060",
- fix completion list contrast:
editorSuggestWidget.selectedBackground OK
list.hoverBackground darkened (slightly)

## [0.0.2]

- fix statusBar.noFolder BG and pkg screenshot url

## [0.0.1]

- Initial release
