# Summerish Theme

Summerish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-summerish_code.png](./images/sjsepan-summerish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-summerish_codium.png](./images/sjsepan-summerish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-summerish_codedev.png](./images/sjsepan-summerish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-summerish_ads.png](./images/sjsepan-summerish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-summerish_theia.png](./images/sjsepan-summerish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-summerish_positron.png](./images/sjsepan-summerish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/13/2025
